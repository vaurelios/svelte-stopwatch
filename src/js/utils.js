/**
 * @param { number} value
 * @returns { string }
 */
export function withPadding(value) {
  return value < 10 ? "0" + value : value.toString();
}
